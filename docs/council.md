---
title: The Council
---

The UFI Council will consist of, at most, one member from each member instance of the UFI. Due to the existence of small and single-member instances there is no assurance that an instance will have representation on the council however. The total number of seats on the council shall be 1/10th (rounded down) the total number of member instances in the UFI. Whenever there is a vacancy an election will be held open to all members of all instances in the UFI to vote. A nomination for a candidate in an election may be carried out by any of the following methods:

- Any administrator may nominate, at most, one member from the UFI community
- Each existing member of the council in good standing may nominate, at most, one member from the UFI community
- Any member of the UFI community may be nominated with the support of at least 50 members of the UFI community

Members of the council may be impeached at any time. An impeachment process must first be initiated and may be done by either a petition from the members of the UFI totaling at least 50 signatures, or by any existing member of the council. If an impeachment is initiated via signature, regardless of the outcome of the impeachment process, none of the those signing the petition for an impeachment hearing shall be allowed to sign another impeachment hearing on the same council member for at least 3 months. This will ensure a small group of people can not filibuster the impeachment process by flooding repeats of the same impeachment hearing repeatedly.

The council will not act as a dictatorship, their only purpose is to keep order and to raise proposals for consideration. Any proposals raised by the council will ultimately come to a full vote among the entire UFI community. Any member of the council may initiate a hearing on any of the following issues:

- Review of an instance member of the UFI for not adhering to the Code of Ethics\sidenote{See section \ref{sec:code_of_ethics}}, potentially resulting in their expulsion from the UFI.
- Impeachment hearing of another UFI council member
- A proposal for an amendment to the UFI's Code of Ethics or other bylaws.

In addition to initiating hearing they will also be in charge of keeping order at such meetings, counting votes, and enacting decisions based on said votes. 

