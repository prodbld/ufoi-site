---
title: Getting Involved
---

here are some alternate "official" ways the conversation should continue:

1) On the official Gitlab repository, which is where all voting and formal discussions will take place

Bylaws: https://gitlab.com/ufoi/constitution
This website: https://gitlab.com/ufoi/ufoi-site

Feel free to simply jump in and make Merge Requests and issues... we discuss as a group so worst case we just reject it. But dont hesitate to make suggestions, large and small.

2) The group on the fediverse (just tag them instead of everyone): [@ufoi@a.gup.pe](https://a.gup.pe/u/ufoi)

3) On matrix chat. [#UFoI:matrix.org](https://matrix.to/#/#UFoI:matrix.org)

And of course you are all welcome to contact council members directly. To see a list of all council members go here: [/council-members](/council-members).
