/**
 * Creating a sidebar enables you to:
 * - create an ordered group of docs
 * - render a sidebar for each doc of that group
 * - provide next/previous navigation.
 *
 * The sidebars can be generated from the filesystem, or explicitly defined here.
 *
 * Create as many sidebars as you want.
 */

module.exports = {
  docs: [
    {
      type: 'category',
      label: 'Introduction',
      items: ['getting-started', 'getting-involved', 'apply-info'],
    },
    {
      type: 'category',
      label: 'How it works',
      items: ['code-of-ethics', 'due-process', 'badges', 'right-to-leave', 'council', 'amendments'],
    },
    {
      type: 'category',
      label: 'Governance',
      items: ['instance-membership', 'council-elections', 'reporting', 'starting-coalitions'],
    },
    {
      type: 'category',
      label: 'Resources',
      items: ['faq', 'bylaws'],
    }
  ],
};
